// SPDX-FileCopyrightText: 2021-2023 Birger Schacht <birger@rantanplan.org>
//
// SPDX-License-Identifier: MIT

use chrono::prelude::*;

pub type ChronoDate = chrono::NaiveDate;
pub type ChronoDateTime = chrono::DateTime<Local>;
